﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flocking_algorithm : MonoBehaviour
{
    public GameObject boidPrefab;
    public static int area = 20;
    static int boidNum = 20;
    public static GameObject[] allBoids = new GameObject[boidNum];
    public static Vector3 center = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < boidNum; i++)
        {
            Vector3 pos = new Vector3(Random.Range(-area, area), Random.Range(-area, area), Random.Range(-area, area));
            allBoids[i] = (GameObject)Instantiate(boidPrefab, pos, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Random.Range(0, 10000) < 50)
        {
            center = new Vector3(Random.Range(-area, area), Random.Range(-area, area), Random.Range(-area, area));
        }
    }
}
