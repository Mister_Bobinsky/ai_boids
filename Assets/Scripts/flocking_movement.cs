﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flocking_movement : MonoBehaviour
{
    public float boidSpeed = 5.0f;

    float boidRotaion = 4.0f;
    float flockDist = 4.0f;

    Vector3 pos;
    Vector3 heading;

    bool boundary = false;
    
    // Start is called before the first frame update
    void Start()
    {
        boidSpeed = Random.Range(5.0f, 10);
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, Vector3.zero) >= flocking_algorithm.area)
        {
            boundary = true;
        }
        else
        {
            boundary = false;
        }

        if (boundary)
        {
            Vector3 direction = Vector3.zero - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), boidRotaion * Time.deltaTime);

            boidSpeed = Random.Range(0.5f, 1);
        }
        else
        {
            if (Random.Range(0, 5) < 1)
            {
                LazyFlocking();
            }
        }

        transform.Translate(0, 0, Time.deltaTime * boidSpeed);
    }

    void LazyFlocking()
    {
        GameObject[] aiBoids;
        aiBoids = flocking_algorithm.allBoids;

        Vector3 boidCenter = Vector3.zero;
        Vector3 boidAvoid = Vector3.zero;
        float speed = 0.1f;

        Vector3 center = flocking_algorithm.center;

        float dist;
        int ctr = 0;

        foreach(GameObject go in aiBoids)
        {
            if(go != this.gameObject)
            {
                dist = Vector3.Distance(go.transform.position, this.transform.position);
                if(dist <= flockDist)
                {
                    boidCenter = boidCenter + go.transform.position;
                    ctr++;

                    if (dist < 1.0f)
                    {
                        boidAvoid = boidAvoid + (this.transform.position - go.transform.position);
                    }

                    flocking_movement flock = go.GetComponent<flocking_movement>();
                    speed = speed + flock.boidSpeed;
                }
            }
        }

        if(ctr > 0)
        {
            boidCenter = boidCenter / ctr + (center - this.transform.position);
            boidSpeed = speed / ctr;

            Vector3 direction = (boidCenter + boidAvoid) - transform.position;
            if(direction != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), boidRotaion * Time.deltaTime);
            }
        }
    }
}
