# ~ San Diego State University - Computer Science 596: 3D Game Design ~

## Assignment02 - Artifical Intelligent Boids - C#: Description

Implements a fully functional 3D simulation which displays flocking behaviour.
Flocking behaviour modes are described here: 
1. Lazy Flight, the flocking AI will choose a random point to fly towards within your scene.
When the destination is reached, a new target within the limits of the scene is chosen
and the flock flies towards that.

Authors: **Roman Mattia**

GitLab: https://gitlab.com/Mister_Bobinsky/ai_boids

**Works Cited:**

- https://assetstore.unity.com/packages/3d/environments/simplistic-low-poly-nature-93894 => Animated Birds
- https://assetstore.unity.com/packages/2d/textures-materials/sky/farland-skies-cloudy-crown-60004 => Skybox Textures
- https://www.youtube.com/watch?v=VCW_T3ZDcBc => Skybox Implementation
- https://www.youtube.com/watch?v=eMpI1eCsIyM => Flocking Research
- https://assetstore.unity.com/packages/audio/music/ou-hito-piano-improvisation-118188 => Music
- https://www.youtube.com/watch?v=mjKINQigAE4&t=1s => Flocking Research
- https://www.youtube.com/watch?v=BKCsH8mQ-lM => Music Implementation
- http://wiki.unity3d.com/index.php?title=Flocking => Flocking Research